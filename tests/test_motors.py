import adafruit_motor.stepper
import adafruit_motorkit
import time
import RPi.GPIO

RPi.GPIO.setup([12, 4], RPi.GPIO.OUT, initial=RPi.GPIO.HIGH) 

#kit = adafruit_motorkit.MotorKit(steppers_microsteps=4)
kit = adafruit_motorkit.MotorKit() 

pump_kit = kit.stepper1
focus_kit = kit.stepper2

def test_pump():
    pump_kit.release()

    # Forward
    for i in range(50):
        pump_kit.onestep(direction=adafruit_motor.stepper.FORWARD, style=adafruit_motor.stepper.DOUBLE)
    time.sleep(1)
    # Backward
    for i in range(50):
        pump_kit.onestep(direction=adafruit_motor.stepper.BACKWARD, style=adafruit_motor.stepper.DOUBLE)
    time.sleep(1)
    pump_kit.release()

def test_focus():
    focus_kit.release()

    # Forward
    for i in range(200):
        focus_kit.onestep(direction=adafruit_motor.stepper.FORWARD, style=adafruit_motor.stepper.DOUBLE)
    time.sleep(1)
    # Backward
    for i in range(200):
        focus_kit.onestep(direction=adafruit_motor.stepper.BACKWARD, style=adafruit_motor.stepper.DOUBLE)
    time.sleep(1)
    focus_kit.release()

if __name__ == '__main__':
    # Test Pump motor
    print("---Test Pump Motor 1/2")
    test_pump()
    # Test Focus motor
    print("---Test Focus Motor 1/2")
    test_focus()
    # Test Pump motor
    print("---Test Pump Motor 2/2")
    test_pump()
    # Test Focus motor
    print("---Test Focus Motor 1/2")
    test_focus()
