# Test led

import time
from signal import signal, SIGINT
from sys import exit
import RPi.GPIO as gpio


def handler(signal_received, frame):
    # Handle any cleanup here
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)

## Blink Mode
BLINK_MODE = False

## Output pin number for led
LED = 21

def init_led(led_pin_number):
    gpio.setmode(gpio.BCM)
    gpio.setwarnings(False)
    gpio.setup(led_pin_number, gpio.OUT)

def close_led(led_pin_number):
    pass

if __name__ == '__main__':
    # Setup led
    init_led(LED)

    # Blink led
    try:
        while True:
            ## Turn on the led
            gpio.output(LED,1)
            if BLINK_MODE:
                time.sleep(2)
                ## Turn off the led
                gpio.output(LED,0)
                time.sleep(2)
    except KeyboardInterrupt:
        gpio.output(LED,0)
        exit(0)
