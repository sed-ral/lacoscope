# Programme Stagiaire 2eme

* Dates 17-21, 24-28 Juin 2024

[toc]


## Stagiaires

* Thomas Favier, Lycée Villard-Bonnot 
* Ambroise Guiot, Lycée Villard-Bonnot
* Audrey Cassagne (1ere semaine), Lycée Voiron
* Claire Tsagouria, Lycée Villard-Bonnot


## Projet Lacoscope

* [Readme](README.md)
* Présentation générale (2h00)

### Tâches

* Présentation générale (2h00)
* S1, Inventaire matériel (1h00)
* S0, Découpe laser (2h00)
* S2, Montage optique (1h00)
* S3-4, Montage aimants/boitier & entretoises (1h00)
* S5, Montage du porte échantillon (1h00)
* S6-7, Montage caméra & LED (2h00)
* S10, Montage du guidage linéaire (1h00)
* S11_verif, Vérifier le support de pompe et le redessiner si besoin (2-4h00)
* S11, Montage de la pompe (30 mns)
* S12-13, Montage électrique carte moteur (1h00)
* S14: Assemblage global (2h00)
* S15: Préparation Raspberry= sd card, montage radiateur, installation (2h00)
* S16-17: Préparation et installation du GPS (30 mns)
* S18 à 22: Montage caméra et ventilateur (30 mns)
* S23-24: Cable plat de connexion entre carte (30 mns)
* S25-28: Montage des flasques (1h00)
* S29-30: Assemblage du circuit fluidique (2h00)

### Tests

* Installation logicielle (2h00)
* Calibration (4h00)
* Test sur échantillons réels (2h00)
* Visite Liphy et présentation (2h00)


## Planning

### Semaine 17-21

#### Lundi 17/06:

* 9h00-10h00: Accueil -> **Ludovic, Roger (service SED)**
* 10h00-11h00: Visite Login -> **Elisa Boutet (service communication et mediation)**
* 14h00-16h00: 

#### Mardi 18/06:

* 10h00-11h00: [réunion SED]
* 14h00-16h00: Visite Kinovis -> **Julien (service SED)**

#### Mercredi 19/06:

* 10h00-11h00: Atelier d'Innovation -> **Stan (service SED)**
* 13h00-13h30: Discussion avec **Sophie Quinton (EPI SPADES)**
* 14h00-16h00: 

#### Jeudi 20/06:

* 10h00-12h00:
* 14h00-16h00: 

#### Vendredi 21/06:

* 11h00-12h00: Discussion avec **Jonathan El Methni (EPI Statify)**
* 14h00-16h00: 

#### Semaine 24-28

#### Lundi 24/06:

* 9h00-12h00: 
* 14h00-16h00: 

#### Mardi 25/06:

* 10h00-11h00: Siconos & devtools, Fabrikariums -> **Maurice & Samuel (service SED)**
* 14h00-16h00: 

#### Mercredi 26/06:

* 10h00-12h00: 
* 14h00-16h00: Plateformes robotiques -> **Nicolas (service SED)**

#### Jeudi 27/06:

* 10h00-12h00:
* 14h00-16h00: 

#### Vendredi 28/06:

* 10h00-12h00: 
* 14h00-16h00: Préparation oral -> **Roger (service SED)**