# Lacoscope - Projet éleves de 2nd 2024


* [Planning stagiaire](https://gitlab.inria.fr/sed-ral/lacoscope/-/blob/main/planningJuin2024.md)

## Dépot fichier lacoscope

- [B1-new_2mm.svg](https://espaces-collaboratifs.grenet.fr/share/s/2NUaK3KfT6GgVCfWUJvUlw)
- [B2-new_3mm.svg](https://espaces-collaboratifs.grenet.fr/share/s/01KFEIu4RfOZ4gqwABsubg)
- [e-new.svg](https://espaces-collaboratifs.grenet.fr/share/s/EUs5KXpUQ12gc5eFFtq97g)
- [i12-new.svg](https://espaces-collaboratifs.grenet.fr/share/s/8h2M4LZiTyyEfSg2DkPHzA)
- [n-new.svg](https://espaces-collaboratifs.grenet.fr/share/s/4cTi24PkTWKAVjd1TYWmXg)
- [opaque_5mm.svg](https://espaces-collaboratifs.grenet.fr/share/s/MmeO3EtXSL6tGpBgLKDBsg)
- [transparent_5mm.svg](https://espaces-collaboratifs.grenet.fr/share/s/xuKHhxGOQni_aTfewnAkZA)

## TODO

* [x] Récupérer lamelle de calibration (Ludo)
* [x] Suivre la commande des moteurs linéaires, aimant 5mm (Roger)
* [x] Sonder SED pour les présentations (Roger)
* [x] Mettre le lien vers la prise de note prototype -> Roger
    * [x] Ajouter taraudage optique = M12 en pas fin 0.5mm 

## Tache à faire (nous ou étudiant)

- [ ] Réalisation d'un plan de cablage électrique
- [ ] Maquette carton
- [ ] Modification support de pompe
- [ ] Modification si nécessaire support optique 
    - filetage M12 ou serrage par écrou M12
- [ ] Modification si nécessaire support moteur deplacement optique

## Modifications des plans/montage

* [ ] Pièces D, M1, M2, M3 (?): passer le trou central en 12 mm
* [ ] Ajout rondelle plastique pour montage caméra: M2x3mm 
* [ ] Montage nouveau moteur: 
    * Redessiner la plaque N pour fixation des chariots
    * Redessiner la plaque E pour le passage des chariots
        * [ ] revoir trou aimant réduire < 5mm trop de jeu
* [ ] Montage nouvelles pièces support de pomep I1 et I2
* [ ] Montage des entretoises carte électronique utiliser des vis M2,5x10 avec entretoise femelle/femelle
* [ ] Pour les pièces collées prévoir des percages d'alignements
    * 4 percages avec des vis M2,5x10 -> ø2mm
* [ ] Réaligner les découpes pour Rasberry en fonction de l'entretoise (pour le moment 10mm) 

Notes : 
- La pièce lacoscope O est le support de la lentille de LED (LED LENSE)
- La pièce lacoscope F est une entretoise pour la lentille de LED
- La pièce B2 : les percages de 
    - l'alim : ø8mm
    - l'antenne GPS : ø6,5mm
- longueur de la nappe de connexion RPI-Controleur 8cm 

## la carte stepper motor

[Stepper Motor HAT from Adafruit](https://learn.adafruit.com/adafruit-dc-and-stepper-motor-hat-for-raspberry-pi/overview)
peut piloter uniquement 2 moteurs. Dans le montage de base les moteur de l'optique son piloté en parralèle. L'autre sortie sert pour la pompe.
Amélioration : Possibilité d'empiler plusieurs carte pour dissocier les moteurs d'optique.
possibilité d'utiliser la version suivante :
[Stepper motor waveshare](https://www.waveshare.com/stepper-motor-hat.htm)

## Liens utiles

* Planktoscope
    * https://www.planktoscope.org/
    * https://github.com/PlanktoScope/PlanktoScope
    * https://www.frontiersin.org/articles/10.3389/fmars.2022.949428/full
    * https://planktoscope.readthedocs.io/en/latest/assembly_guide/
    * Dernière version avec améliorations [v2.5](https://docs-edge.planktoscope.community/setup/hardware/v2.5/assembly/)
* Lacoscope (le projet local du liphy/uga/cnrs)
    * [site institutionnel](https://lacoscope.cnrs.fr/)
    * [espace collaboratif](https://espaces-collaboratifs.grenet.fr/share/page/site/lacoscope/dashboard/)
    * [read the doc](https://project-lacoscope.readthedocs.io/en/latest/)
    * [github](https://github.com/lacoscope/PlanktonScope)
* Pompes [Kamoer fluid tech](https://www.kamoer.com/us/product/index.html?categoryProductId=1)
    * pompe brushless mais avec control PWM et feedback,peut être une solution moins chère KPHM100-HE (12V, 0.5A) [amazon](https://www.amazon.com/Kamoer-KPHM100-peristaltic-brushless-adjustable/dp/B0BYCY1HXH)

## Liste matériel

[Liste planktoscope V2.1](https://www.planktoscope.org/replicate/get-your-kit)

[Liste lacoscope](https://github.com/lacoscope/PlanktonScope/blob/main/docs/assembly_guide.md) site GitHub See Step1. Based on PlanktoScope V2.3

### Check list matériel pour 1 unité

Electonics & Actuators:

- [ ] Raspberry Pi 4 (5V 2.5A)
- [ ] Kit radiateur Pi 4
- [ ] **LiPhy** Motor HAT (this module controls 3 steppers: 2 for thr focus, 1 for the pump)
- [ ] **Liphy** GPS HAT
- [ ] **Liphy** Fan HAT
- [ ] 2 GPIO ribbon femelle connector
- [ ] flat ribbon cables
- [ ] ~~DC Power Jack Socket~~ -> alim double
- [ ] **Liphy** GPS antenna 
- [ ] cable uFL to SMA adapter (female-female TODO quel longeur ?)
- [ ] SD card + Adapter (in order to flash the OS image)
- [ ] **En attente** stepper motor (5-12V 1.2A) + commande AliExpress
- [ ]  Alimentation double 5V (4A) et 12V (2A)
- [ ] Selon alimentation un convertisseur 12V@6A vers 5V@3A
- [ ] 2 Stacking header femelle 2x20
- [ ] 2 Hammer header male 2x20
- [ ] Stepper Motor Peristaltic Pump **TODO ajouter référence** (12V 1.7A)
- [ ] **Liphy** 2 mini écrans

Fluidics:

- [x] **Liphy** Tanks HSW 20ml Syringe
- [x] **Liphy** 1m Silicone tubing ID 1.6 mm
- [x] **Liphy** 2 Luer Lock Connector Female 1.6 mm
- [x] **Liphy** 2 Luer Connector Male 1.6 mm
- [x] **Liphy** µ-Slide I Luer Variety Pack

Optics:

- [x] **Liphy** White LED 5 mm
- [x] M12 lens 25 mm
- [x] M12 lens 16 mm
- [x] bague de serrage M12 dans plastique (pb taraud pas de vis optique)
- [x] Camera Pi V2.1
- [x] flex cable (longueur à déterminer : 40 ou 60cm)
- [ ] **CMD ludo** lamelle de calibration pour microscopie

Other material:

- [x] Standoff (entretoise TODO a préciser) M2.5 6mm
- [x] Standoff (entretoise TODO a préciser) M2.5 12mm
    - Attention entretoise male/femelle je pense le mieux est de prendre un kit ici https://www.amazon.com/gp/product/B01N5RDAUX/
- [x] M2x8mm
- [x] M3x12mm screws 
- [x] M3 nuts
- [x] magnets diametre 6mm
- [x] CR1220 Battery
- [x] Transparent acrylic
- [x] Black acrylic



### MISC - A commander	


* [x] 2 Flex Cable for Pi Camera 60 cm, [amazon](https://www.amazon.fr/AZDelivery-Remplacement-cam%C3%A9ra-Raspberry-arduino/dp/B075JNBNRM/ref=sr_1_3_ssp) pas trop long. 30 cm semblerais suffisant 

* 2 Pompes peristaltics à moteur pas à pas :
    * ~~12V XP88-ST01; 1.0A 0-88mL/min [amazon plus en stock](https://www.amazon.com/Peristaltic-Metering-Stepper-Motor-0-88mL/dp/B01LY35CKH/)~~
    * [x] 12V Kamoer KAS-12V (2.5IDx4.5OD) 43€/PU + 0€ port (total 86€ les deux)[amazon](https://www.amazon.com/peristaltic-Stepper-Kamoer-Variable-OD%EF%BC%8C5-84ml/dp/B094QHDSQL/?th=1) => 15-24 Mai
    * ~~si on trouve la tête de pompe, nous pourrions adapter les moteurs avec ceux en stock au liphy voir ci dessous~~
    * ~~Pompe péristaltique KAS (model planktoscope 2.5) [a2v.fr](https://www.a2v.fr/kamoer/pompe-peristaltique-kas.php)~~
    * autre pompe je ne sais pas ce que cela vaut [aliexpress](https://fr.aliexpress.com/item/4000929706373.html)
    
* Actionneurs linéaires 80mm :
    * [x] 4 Linear Stepper Motor basique, [amazon](https://www.amazon.fr/curseur-lin%C3%A9aire-Machine-bricolage-D8-MOTOR80/dp/B08XWPV7B6/ref=sr_1_4) 
    * [x] 2 linear stepper motor renforcé, [aliexpress](https://fr.aliexpress.com/item/4000834477859.html)
* [x] 2 MicroSD Card + Adapter 32 Go, [amazon](https://www.amazon.fr/gp/product/B06XWMQ81P/ref=ppx_yo_dt_b_asin_title_o07_s00)
* Optiques soit le kit + l'objectif de focal 25 soit pour réduire le coût uniquement les focal 16 et 25 (cf version planktoscope 2.5).
    * ~~2 Arducam M12 Lens Kit (focal de 1.7 à 20mm), [amazon](https://www.amazon.com/gp/product/B07L92S9MT/ref=ox_sc_act_title_1?smid=A2IAB2RW3LLT8D&psc=1), 89€ le kit~~
    * [x] 2 M12 Lens IR 1/2" 5MP (focal 16mm), [amazon](https://www.amazon.com/5MP-Action-Camera-Fixed-M12-Distance/dp/B079LCN2DJ)
    [amazonfr](https://www.amazon.fr/gp/product/B07VXH9P5G/ref=ox_sc_act_title_1?smid=A19HXCV96LJNAY&psc=1)
    * [x] 2 M12 Lens IR 1/2" 5MP (focal 25mm), [amazon](https://www.amazon.fr/Lazmin-Objectif-vid%C3%A9osurveillance-Remplacement-Surveillance/dp/B07WGH9TLT/ref=sr_1_1)
    * [x] 1 kit de bague M12 pour faciliter la fixation des objectifs (cf planktoscope 2.5) [amazon](https://www.amazon.fr/Lot-bagues-serrage-fixes-plastique/dp/B09WGLX4XH/ref=sr_1_4)

* [x] 2 CR1220 Battery, [amazon](https://www.amazon.fr/gp/product/B06XQ1C5TN/ref=ppx_yo_dt_b_asin_title_o07_s01)
* [x] 32 Magnets 6 x 2 mm [amazon](https://www.amazon.fr/aimants-cylindriques-r%C3%A9frig%C3%A9rateur-magn%C3%A9tique-BTLIN/dp/B07RT9MY52/ref=sr_1_5)
* [x] kit radiateur pi [amazon](https://www.amazon.fr/VooGenzek-Dissipateurs-Raspberry-Dissipateur-Thermo-Conducteur/dp/B0C2HW56YJ/ref=sr_1_4)
* [x] Plaques transparente et noire 5mm, dispo.

### Inria

* 2 Raspberry Pi 4 B (4GB) 
* 2 DC Power Jack Socket 
* 2 GPIO Ribbon IDC 40P [amazon](https://www.amazon.com/dp/B0762XXV62/ref=sspa_dk_detail_1?th=1)
* 2 Micro HDMI Cable
* A vérifier au labo d'élec:
  * 2 barettes Adafruit Hammer Header 
  * 2 Pitch IDC Sockets 2x20
  * 2 Male Pitch IDC Sockets 2x20 (Si besoin [amazon](https://www.amazon.com/gp/product/B00K2NTSJE/ref=ppx_yo_dt_b_asin_title_o09_s00?ie=UTF8&psc=1) )
  * 2 kit radiateurs rpi4 [amazon](https://www.amazon.com/Raspberry-Heatsink-Conductive-Adhesive-Aluminum/dp/B07YR6M6F6?th=1)
* A vérifier pour les alims:
  * 2 alim 5v 3A pour les moteurs avec 1 convertisseur USB-5v/12v ??
  * 2 alim 5v 1A pour le raspi
* A vérifier Vis:
  * M2x8mm and M3x12mm screws and M3 nuts 
  * Colonnette femelle M2.5 6mm and 15mm 
 

### Stock Liphy

* 4 Adafruit Stepper Motor HAT
* 4 Adafruit Ultimate GPS HAT
* 4 GPS Active Antenna	
* 2 Yahboom Cooling Fan HAT
* Une dizaine de LED white 5mm
* 3 cartes ventilateur Yahboom Raspberry Pi Quiet Cooling Fan for Raspberry Pi
* 2 mini écrans
* 2 moteurs 14HS13-0406S OSM20170305 
    * Nema 14 présentant un couple de maintien de 10 Ncm 10V 0.4A 
    * [gotronic](https://www.gotronic.fr/art-moteur-14hs13-0406s-18352.htm)
* Composants fluidique :
  * 2 µ-Slide I Luer Variety Pack ok
  * 4 HSW 20ml Syringe ok
  * 2x1m Silicone Tubing ID 1.6mm
  * Luer Connector Male
  * Luer Connector Male 1.6 mm
* 2 camera HD raspi

