## Doc to follow

- https://project-lacoscope.readthedocs.io/

## Raspberry image and system setting
- Install on distribution : `
    - Hardware configuration : ```Raspberry Pi 4 Model B``` + ```adafruithat```
    - OS ```Raspbian GNU/Linux 12 32 bits```
- Create ```pi``` user !!!
    - Planktoscope Python scripts point explicitely to  ```/home/pi``` directory !

- Set up raspberry config (check config in https://github.com/lacoscope/PlanktonScope/blob/main/docs/expert_setup.md)
```shell
$ sudo raspi-config
## Password pi
## Hostname lacoscope-inria[1-2]
```

## Clone git repo

```shell
$ git clone https://github.com/lacoscope/PlanktonScope
```

- Important :
    - DO NOT clone code on ~~https://github.com/PlanktoScope~~
    - Instructions not ok and need to be changed :
        - https://github.com/lacoscope/PlanktonScope/blob/main/docs/expert_setup.md
        - file not found ```requirements.txt```! 
           - Use the file requirements.txt proposed in this repository !


```shell
$ cd PlanktonScope/scripts/raspian_configuration/
$ cp etc/systemd/system/autohotspot.* /etc/system/system/
```
## Apt install

- [x] python3 python3-venv python3-pip
- [x] python3-opencv
- [x] i2c-tools libgpiod-dev python3-libgpiod
- [x] gpsd gpsd-clients
- [x] pps-tools
- [x] chrony
- [x] mosquitto, mosquitto-clients
- [x] nginx, libnginx-mod-http-xslt-filter
- [x] ftp
- [x] cmake

## Pip install

- On new version of raspberry OS, ```pip install``` need to be used through a virtual environment
   - BEWARE need to use ```venv``` !!

```shell
$ python3 -m venv $HOME/venv --system-site-packages
$ source $HOME/venv/bin/activate
(venv)$ pip3 install --upgrade -r requirements.txt
(venv)$ wget https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/raspi-blinka.py
(venv)$ sudo -E env PATH=$PATH python3 raspi-blinka.py
```

## Userland old video libraries

For now Planktoscope refers to ```raspimjpeg``` binary (cf scripts/raspimjpeg/) that needs libmmal libraries.
These libraries seem to be obsolete with newer raspberry pi OS. But userland repository proposes the source code needed to build these libraries. 
   - TOBEDONE : replace the ```raspimjpeg``` binaries ?

```shell
$ cd /home/pi/
$ git clone https://github.com/raspberrypi/userland.git
$ cd userland.git
$ mkdir build; cd build
$ cmake ..
# check you get lib/libmmal_core.so and lib/libmmal.so
$ sudo cat "/home/pi/userland/build/lib" > /etc/ld.so.conf.d/userland.conf
$ sudo ldconfig
# then the location of libmmal_core.so and libmmal.so is know system wide
```
## Install Node-RED

```shell
$ bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)
```
(type `y` to all)

```shell
$ sudo mkdir -p /etc/systemd/system/nodered.service.d/
$ sudo cp /home/pi/PlanktonScope/scripts/raspbian_configuration/etc/systemd/system/nodered.service.d/override.conf /etc/systemd/system/nodered.service.d/override.conf
$ sudo systemctl daemon-reload
$ sudo systemctl enable nodered.service
# Check on browser : http://localhost:1880
```

### Integrate Planktoscope into node-red

- **Beware : no need to perform modif on file /home/pi/.node-red/settings.js**
   cf https://github.com/lacoscope/PlanktonScope/blob/main/docs/expert_setup.
   md#install-the-necessary-nodes-and-activate-necessary-features

<code>
   # change the following lines (you can use CTRL+_ to quickly navigate to the line indicated):
   #Line 75: uncomment the line (remove the //) that ends with flowFilePretty: true,
   #Line 337: set enabled to true
   ==> already set !
</code>

- Integrate PlanktonScope into node-red architectures

```shell
$ cd /home/pi/.node-red/projects
$ mv /home/pi/PlanktonScope/ .
$ ln -s /home/pi/.node-red/projects/PlanktoScope/ /home/pi/PlanktonScope
$ sudo systemctl restart nodered.service
$ cd /home/pi/.node-red/
$ npm install copy-dependencies
# retrieve dependencies and update package.json file
$ node_modules/copy-dependencies/index.js projects/PlanktonScope ./
$ npm update
```

**BEWARE** : 
- Error on file https://github.com/lacoscope/PlanktonScope/blob/main/docs/expert_setup.md
    - line 688 => missing or not necessary line because link instruction is already given above !

- ** When using http://localhost:1880/ui : errors related to missing dependencies, check the above error message
<code>
Flux arrêtés en raison de types de noeuds manquants.

fs-file-lister
base64
</code>
   - add package node-red-node-base64 and node-red-contrib-fs
```shell
$ npm install node-red-node-base64
$ npm install node-red-contrib-fs
```

### MISC Security settings

No need to perform the following settings, unless you set a password when installing node-red
Check below on how to disable it if you do not need authentification.

------------
This has been done at init but disable since !!!
```shell
  username : raspi 
  mdp : mr=raspi
  
  # Setting written on file 
  /home/pi/.node-red/settings.js
```
Comment section **adminAuth** of this file to disable the login/mdp
------------

## GPS setup

- Change ```/etc/defautl/gpsd``` and restart gpsd service

```shell
$ cat /etc/default/gpsd
# Devices gpsd should collect to at boot time.
# They need to be read/writeable, either by user gpsd or the group dialout.
DEVICES="/dev/serial0"

# Other options you want to pass to gpsd
GPSD_OPTIONS="-n -r"

# Automatically hot add/remove USB GPS devices via gpsdctl
USBAUTO="false"

$ sudo systemctl restart gpsd.service

$ gpsmon # to decode GPS messages, need the antennq !

$ sudo nano /boot/firmware/config.txt
# Add the following line at the end of the line
dtoverlay=pps-gpio,gpiopin=4

$ sudo nano /etc/modules
# Add the following line at the end of the line
pps-gpio

$ sudo vi /etc/default/gpsd
# we need to add our pps device to the line DEVICES. Append /dev/pps0:
DEVICES="/dev/serial0 /dev/pps0"

$ sudo ppstest /dev/pps0s
```

## Chrony

```shell
$ chronyc sources -v
 .-- Source mode  '^' = server, '=' = peer, '#' = local clock.
 / .- Source state '*' = current best, '+' = combined, '-' = not combined,
| /             'x' = may be in error, '~' = too variable, '?' = unusable.
||                                                 .- xxxx [ yyyy ] +/- zzzz
||      Reachability register (octal) -.           |  xxxx = adjusted offset,
||      Log2(Polling interval) --.      |          |  yyyy = measured offset,
||                                \     |          |  zzzz = estimated error.
||                                 |    |           \
MS Name/IP address         Stratum Poll Reach LastRx Last sample               
===============================================================================
#x NMEA                          0   4   377    15  +45488h[+45488h] +/-  125us
#? NME+                          0   4     0     -     +0ns[   +0ns] +/-    0ns
#? SOCK                          0   4     0     -     +0ns[   +0ns] +/-    0ns
^+ 185.254.100.25                3   6   377    56  -1199us[-1199us] +/-   35ms
^+ likho.oneyed.monster          3   9   377   119    +45us[  +45us] +/-   35ms
^+ 27.ip-51-68-44.eu             3   9   377   114  -1248us[-1248us] +/-   34ms
^* ntp.tuxfamily.net             3   7   377   387  -1063us[-1128us] +/-   61ms
```

## Modif code

This section is obsolete (unless your $HOMEDIR on raspberry is different from ```/home/pi```)
----------------
- Need to change location 
   - from /home/pi/Planktoscope to /home/raspi/Lacoscope/PlanktonScope
      - flows/main.json
      - scripts/raspimjpeg/raspimjpeg.conf
      - scripts/planktoscope/imager/raspimjpeg.py
      - scripts/planktoscope/imager/__init__.py
      - scripts/planktoscope/stepper.py
      - scripts/planktoscope/segmenter/__init__.py
      - scripts/bash/update.sh
      - scripts/bash/start_update.sh
      - scripts/bash/ftp_connect.sh
----------------

## Test components

Test files are available on ```tests``` directory located on this repository

## Test Opencv + Morphocut version
```shell
(venv) $ python3
Python 3.11.2 (main, Mar 13 2023, 12:18:29) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import cv2
>>> cv2.__version__
'4.10.0'
>>> import morphocut
>>> morphocut.__version__
'0.1.2'
```

### I2C 


```shell
$ sudo i2cdetect -y 1

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:                         -- -- -- -- -- 0d -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- 3c -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: 60 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: 70 -- -- -- -- -- -- --                         
```
=> OK


```shell
(venv)$ python3 tests/blinkatest.py 
```
=> OK

### Test camera

```shell
(venv) $ libcamera-still -o test_acqui_camera.jpg
0:11:23.447882225] [2334]  INFO Camera camera_manager.cpp:284 libcamera v0.2.0+120-eb00c13d
[0:11:23.490202701] [2340]  WARN RPiSdn sdn.cpp:40 Using legacy SDN tuning - please consider moving SDN inside rpi.denoise
[0:11:23.492102178] [2340]  WARN RPI vc4.cpp:392 Mismatch between Unicam and CamHelper for embedded data usage!
[0:11:23.492682134] [2340]  INFO RPI vc4.cpp:446 Registered camera /base/soc/i2c0mux/i2c@1/imx219@10 to Unicam device /dev/media3 and ISP device /dev/media0
[0:11:23.492733467] [2340]  INFO RPI pipeline_base.cpp:1102 Using configuration file '/usr/share/libcamera/pipeline/rpi/vc4/rpi_apps.yaml'
Made X/EGL preview window
Mode selection for 1640:1232:12:P
    SRGGB10_CSI2P,640x480/0 - Score: 4504.81
    SRGGB10_CSI2P,1640x1232/0 - Score: 1000
    SRGGB10_CSI2P,1920x1080/0 - Score: 1541.48
    SRGGB10_CSI2P,3280x2464/0 - Score: 1718
    SRGGB8,640x480/0 - Score: 5504.81
    SRGGB8,1640x1232/0 - Score: 2000
    SRGGB8,1920x1080/0 - Score: 2541.48
    SRGGB8,3280x2464/0 - Score: 2718
Stream configuration adjusted
[0:11:24.632134290] [2334]  INFO Camera camera.cpp:1183 configuring streams: (0) 1640x1232-YUV420 (1) 1640x1232-SBGGR10_CSI2P
[0:11:24.632647469] [2340]  INFO RPI vc4.cpp:621 Sensor: /base/soc/i2c0mux/i2c@1/imx219@10 - Selected sensor format: 1640x1232-SBGGR10_1X10 - Selected unicam format: 1640x1232-pBAA
Mode selection for 3280:2464:12:P
    SRGGB10_CSI2P,640x480/0 - Score: 10248.8
    SRGGB10_CSI2P,1640x1232/0 - Score: 6744
    SRGGB10_CSI2P,1920x1080/0 - Score: 6655.48
    SRGGB10_CSI2P,3280x2464/0 - Score: 1000
    SRGGB8,640x480/0 - Score: 11248.8
    SRGGB8,1640x1232/0 - Score: 7744
    SRGGB8,1920x1080/0 - Score: 7655.48
    SRGGB8,3280x2464/0 - Score: 2000
[0:11:29.767362092] [2334]  INFO Camera camera.cpp:1183 configuring streams: (0) 3280x2464-YUV420 (1) 3280x2464-SBGGR10_CSI2P
[0:11:29.772836397] [2340]  INFO RPI vc4.cpp:621 Sensor: /base/soc/i2c0mux/i2c@1/imx219@10 - Selected sensor format: 3280x2464-SBGGR10_1X10 - Selected unicam format: 3280x2464-pBAA
Still capture image received
```
=> OK

### Test led

```shell
(venv)$ python3 tests/test_led.py
```

### Test GPS

```shell
(venv)$ stty -F /dev/serial0 raw 9600 cs8 clocal -cstopb
(venv)$ cat /dev/serial0
GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010737.094,,,,,0,0,,,M,,M,,*59
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GPGSV,1,1,00*79
$GLGSV,1,1,00*65
$GNRMC,010737.094,V,,,,,0.00,0.00,060180,,,N*53
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010738.094,,,,,0,0,,,M,,M,,*56
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GNRMC,010738.094,V,,,,,0.00,0.00,060180,,,N*5C
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010739.094,,,,,0,0,,,M,,M,,*57
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GNRMC,010739.094,V,,,,,0.00,0.00,060180,,,N*5D
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010740.094,,,,,0,0,,,M,,M,,*59
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GNRMC,010740.094,V,,,,,0.00,0.00,060180,,,N*53
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010741.094,,,,,0,0,,,M,,M,,*58
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GNRMC,010741.094,V,,,,,0.00,0.00,060180,,,N*52
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010742.094,,,,,0,0,,,M,,M,,*5B
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GPGSV,1,1,00*79
$GLGSV,1,1,00*65
$GNRMC,010742.094,V,,,,,0.00,0.00,060180,,,N*51
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010743.094,,,,,0,0,,,M,,M,,*5A
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GNRMC,010743.094,V,,,,,0.00,0.00,060180,,,N*50
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010744.094,,,,,0,0,,,M,,M,,*5D
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GNRMC,010744.094,V,,,,,0.00,0.00,060180,,,N*57
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010745.094,,,,,0,0,,,M,,M,,*5C
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GNRMC,010745.094,V,,,,,0.00,0.00,060180,,,N*56
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010746.094,,,,,0,0,,,M,,M,,*5F
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GNRMC,010746.094,V,,,,,0.00,0.00,060180,,,N*55
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010747.094,,,,,0,0,,,M,,M,,*5E
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GPGSV,1,1,00*79
$GLGSV,1,1,00*65
$GNRMC,010747.094,V,,,,,0.00,0.00,060180,,,N*54
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010748.094,,,,,0,0,,,M,,M,,*51
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GNRMC,010748.094,V,,,,,0.00,0.00,060180,,,N*5B
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010749.094,,,,,0,0,,,M,,M,,*50
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GNRMC,010749.094,V,,,,,0.00,0.00,060180,,,N*5A
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
$GNGGA,010750.094,,,,,0,0,,,M,,M,,*58
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GLGSA,A,1,,,,,,,,,,,,,,,*02
$GNRMC,010750.094,V,,,,,0.00,0.00,060180,,,N*52
$GNVTG,0.00,T,,M,0.00,N,0.00,K,N*2C
[...]
```

### Test nging

```shell
$ service nginx status
```
Or in a browser open : ```https://localhost:80```

## MISC

### Git python package installation
*** BEWARE NOT NEEDED for OUR INSTALL : only for pscope_hat version=> ours is ada fruit !!***
```shell
(venv)$ git clone https://github.com/ZJAllen/ShushEngine.git

(venv)$ cd ShushEngine
(venv)$ python3 setup.py install
```
